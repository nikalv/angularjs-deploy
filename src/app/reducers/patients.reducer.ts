import { PATIENTS } from '../constants/patients';

const initialState = [];

export function PatientsReducer(state = initialState, action) { 
    switch (action.type) {
        case PATIENTS.ADD_PATIENT:
            return [...state, action.payload];
        case PATIENTS.SET_PATIENTS:  
            return action.payload;
        case PATIENTS.EDIT_PATIENT:
        return state.map(patient =>
            (patient.id === action.payload.id) ? action.payload : patient);
        case PATIENTS.ADD_PATIENT_DIAGNOSE:
            return state.map(patient => {
              return (patient.id === action.payload.id) ? action.payload : patient;
            });
        case PATIENTS.REMOVE_ACTIVE_MEDICATION: 
            return state.map(patient =>
              (patient.id === action.payload.id) ? action.payload : patient);
        case PATIENTS.UPDATE_MEDICATION: 
            return state.map(patient =>
                (patient.id === action.payload.id) ? action.payload : patient);
        case PATIENTS.UPDATE_DIAGNOSES: 
            return state.map(patient =>
                (patient.id === action.payload.id) ? action.payload : patient);
        case PATIENTS.ADD_PATIENT_MEDICATION: 
            return state.map(patient =>
                (patient.id === action.payload.id) ?
                action.payload
                : patient
              );
        default:
            return state;
    }
}
