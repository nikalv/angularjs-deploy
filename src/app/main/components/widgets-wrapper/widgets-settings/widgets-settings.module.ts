import * as angular from 'angular';
import { NgModule } from 'angular-ts-decorators';
import { widgetsSettingsComponent } from './widgets-settings.component';

@NgModule({
  id: 'widgetsSettingsModule',
  declarations: [widgetsSettingsComponent]
})

export class widgetsSettingsModule {

}
