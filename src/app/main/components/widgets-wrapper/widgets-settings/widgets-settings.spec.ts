import { } from 'jasmine';
import * as angular from 'angular';
import 'angular-mocks';
import * as _ from 'lodash';
import './widgets-settings.module';
import ngMaterial  = require ('angular-material');

describe('widgetsSettingsModule', () => { 
  let scope;
  let componentController: any;
  let ctrl: any;
  const $mdDialog = {show: () => {}, hide: ()=>{} };   
  let list =["diagnoses", "medications"];

  beforeEach(angular.mock.module('widgetsSettingsModule', ngMaterial));   
  beforeEach(angular.mock.module(function ($provide) {       
    $mdDialog.show = jasmine.createSpy('');
    $mdDialog.hide = jasmine.createSpy('');    
  }));
  beforeEach(angular.mock.inject(($rootScope, _$componentController_) => {
    scope = $rootScope.$new();
    componentController = _$componentController_;
    ctrl = componentController('widgetsSettingsComponent', { $scope: scope });
  }));

  it('should check if item exists', () => {
    expect(ctrl.exists('diagnoses', list)).toBeTruthy();
    expect(ctrl.exists('Dia', list)).toBeFalsy();
  });
 
  it('selected arrray  must contain  "diag" ', () => {
    const item ="diag"; 
    ctrl.toggleWidgetsCheckbox(item);
    expect(ctrl.selected).toContain('diag');  
  });

  it('selected arrray  must not contain  "diagnoses" ', () => {
    const item ="diagnoses"; 
    ctrl.toggleWidgetsCheckbox(item);
    expect(ctrl.selected).not.toContain('diagnoses');  
  });

  it('check if mdDialog was called', () => {
   ctrl.showTabDialog();
   expect($mdDialog.show).toHaveBeenCalled;  
  });
   
  it(' checking if $mdDialog.hide function was called', () => {
    const state = true;
    ctrl.toggleDialog(state);
    expect($mdDialog.hide).toHaveBeenCalled;   
   });

   it('toggleDialog function checking if state is true', () => {
    ctrl.toggleDialog(true);
    expect(ctrl.widgetsStatus).toBe(true);   
   });
});