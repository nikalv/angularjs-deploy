import * as _ from 'lodash';
import { Injectable } from 'angular-ts-decorators';

@Injectable()
export class WidgetTableService {

  getGridOptions(widget, index, property) {
    return { 
      enableCellEditOnFocus: true,
      enableHorizontalScrollbar: 0,
      enableVerticalScrollbar: 0,
      rowHeight: 110,
      enableSorting: false,
      enableColumnMenus: false,
      columnDefs: this.getColumnDefs(widget, index, property)
    };
  }

  getTableData(widget, index, data) {
    return (widget === 'diagnoses') ? `${widget}[${index}].${data}` : `${widget}.active[${index}].${data}`;
  }
  
  getColumnDefs(widget, index, property) {
    const columnDate = this.getTableData(widget, index, 'date');
    const columnProperty = this.getTableData(widget, index, property);
    return [
      {
        name: columnDate, 
        enableCellEdit: false, 
        displayName: 'date', 
        width: 85, 
        cellClass: 'blue', 
        enableHiding: false
      },
      {
        name: columnProperty, 
        enableCellEditOnFocus: false, 
        displayName: property, 
        width: 150, 
        enableHiding: false
      }
    ];
  } 
}

