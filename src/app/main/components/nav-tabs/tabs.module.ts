import * as angular from 'angular';
import { tabsRoutes } from './tabs.config';
import { TabsComponent } from './tabs.component';
import { TabsList } from './tabs.constant';
import { diagnosesModule } from '../diagnoses/diagnoses.module';
import { PatientInfoModule } from '../patient-info/patient-info.module';


export const tabsModule = angular
  .module('app.main.tabs', [
    PatientInfoModule.name,
    diagnosesModule
  ])
  .component('tabsComponent', TabsComponent)
  .constant('TabsProperties', TabsList.Default)
  .config(tabsRoutes)
  .name;
