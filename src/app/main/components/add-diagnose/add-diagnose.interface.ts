export interface NewDiagnose {
  id: number;
  name: string;
  diagnose: string;
  date: Date;
  resolved: string;
}
