import * as angular from 'angular';
import { NgModule } from 'angular-ts-decorators';
import { AddDiagnoseComponent } from './add-diagnose.component';

@NgModule({
  declarations: [AddDiagnoseComponent]
})

export class AddDiagnoseModule {

}

