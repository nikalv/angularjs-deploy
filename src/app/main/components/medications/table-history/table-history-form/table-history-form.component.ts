import * as angular from 'angular';
import { Component,  OnChanges, OnInit, Output } from 'angular-ts-decorators';
import { TableFormHistoryService } from './table-history-form.service';
import { EditedMeds } from './table-history-form.interface';

const templateUrl = require('./table-history-form.component.html');

@Component({
  selector: 'table-history-form-component',
  template: templateUrl
})

export class TableHistoryFormComponent implements OnInit {
  @Output() onSaveMeds;

  static $inject = ['TableFormHistoryService','$mdDialog', '$mdMenu'];
  editedMeds: EditedMeds;

  constructor(
    private TableFormHistoryService: TableFormHistoryService,
    private $mdDialog: any,
    private $mdMenu: any,
  ){}

  ngOnInit() {
    this.editedMeds = this.TableFormHistoryService.getEditedMeds();
  }
    
  onSubmit() {
    this.onSaveMeds({
      $event: {
        editedMeds: this.editedMeds
      }
    });
  }

  announceClick() {
    this.$mdDialog.show(
      this.$mdDialog.alert()
        .textContent('Press SAVE to save changes?')
        .ok('Ok')
    );
  };      
}

      
      
    