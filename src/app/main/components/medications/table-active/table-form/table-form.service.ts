import { Injectable } from 'angular-ts-decorators';

@Injectable()
export class  TableFormService {
  static $inject = ['$http'];
    
  constructor(private $http: ng.IHttpService) {}
  
  public getEditedMeds() {
    return {
      startDate: '',
      status: 'active',
      doctor: '',
      stopDate: '',
      by: '',
      drugs: ''
    };
  }
}

