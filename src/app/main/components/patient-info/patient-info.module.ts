import { PatientInfoComponent } from './patient-info.component';
import { PatientIconColor } from './patient-info.directive';
import { NgModule } from 'angular-ts-decorators';
import { WidgetsWrapperModule } from '../widgets-wrapper/widgets-wrapper.module';
import {  PatientEditModule} from '../patient-edit/patient-edit.module';
@NgModule({
  id: 'PatientInfoModule',
  declarations: [PatientInfoComponent, PatientIconColor],
  imports: [WidgetsWrapperModule.name,PatientEditModule.name]
})
export class PatientInfoModule {}




