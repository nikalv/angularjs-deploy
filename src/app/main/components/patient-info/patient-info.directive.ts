
import { Directive} from 'angular-ts-decorators';
export interface IMyScope extends ng.IScope {
  backgroundColor: '@color';
}

@Directive({
  restrict: 'A',
  selector: 'patient-icon-color',
  link: ($scope: IMyScope, $element, $attrs) => {
  $element.css ('background-color', $scope.backgroundColor);
  $attrs.$observe ('color', (value: any) => {
   $element.css ('background-color', value);
    }); 
  }
})

export class PatientIconColor implements ng.IDirective { }


  

  


