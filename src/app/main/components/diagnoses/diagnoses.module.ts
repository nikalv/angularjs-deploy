import * as angular from 'angular';
import { diagnosesRoutes } from './diagnoses.config';
import { DiagnosesComponent } from './diagnoses.component';
import { AddDiagnoseModule } from '../add-diagnose/add-diagnose.module';
import { DiagnosesTablesModule } from './diagnoses-tables/diagnoses-tables.module';

export const diagnosesModule = angular
  .module('app.main.diagnoses', [
    AddDiagnoseModule.name,
    DiagnosesTablesModule.name
  ])
  .component('diagnosesComponent', DiagnosesComponent)
  .config(diagnosesRoutes)
  .name;


