import { NgModule } from 'angular-ts-decorators';
import { LoginComponent } from './login.component';
import { LoginService } from './login.service';
import ngMaterial = require('angular-material');
import 'angular-material/angular-material.css';
import * as NgMessageseModule from 'angular-messages';


@NgModule({
  id: 'LoginModule',
  imports: [ngMaterial, NgMessageseModule],
  declarations: [ LoginComponent ],
  providers: [
    { provide: 'LoginService', useClass: LoginService }
  ]
})

export class LoginModule { }


