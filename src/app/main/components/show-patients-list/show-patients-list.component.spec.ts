import { } from 'jasmine';
import * as angular from 'angular';
import 'angular-mocks';
import * as _ from 'lodash';
import './show-patients-list.module';

describe('Show Patients List Component', () => {

  let scope;
  let componentController: any;
  let ctrl: any;
  const mockPatients = [
    {
      id: 1,
      firstName: 'John',
      lastName: 'Doe',
      sex: 'male',
    },
    {
      id: 2,
      firstName: 'Ann',
      lastName: 'Bureau',
      sex: 'female',
    },
    {
      id: 3,
      firstName: 'Linda',
      lastName: 'Kaufman',
      sex: 'female',
    }
  ];
  const mockSortedPatients = [
    {
      id: 2,
      firstName: 'Ann',
      lastName: 'Bureau',
      sex: 'female',
    },
    {
      id: 1,
      firstName: 'John',
      lastName: 'Doe',
      sex: 'male',
    },
    {
      id: 3,
      firstName: 'Linda',
      lastName: 'Kaufman',
      sex: 'female',
    }
  ];

  beforeEach(angular.mock.module('ShowPatientsListModule'));

  beforeEach(angular.mock.inject(($rootScope, _$componentController_) => {
    scope = $rootScope.$new();
    componentController = _$componentController_;
    ctrl = componentController('showPatientsListComponent', { $scope: scope });
  }));

  it('should call onSelectPatient method', () => {

    ctrl.onSelectPatient = (index)=>{
      this.index = index;
    }
    spyOn(ctrl, 'onSelectPatient');
    ctrl.selectPatient(2);

    expect(ctrl.onSelectPatient).toHaveBeenCalledTimes(1);
  });

  it('should set sorted patients when changes are done', () => {

    ctrl.showPatientsListService.sortPatientsList = (mockPatients) => {
      return mockSortedPatients;
    };

    ctrl.ngOnChanges({ patients: { currentValue: mockPatients } });

    expect(ctrl.sortedPatients).toEqual(mockSortedPatients);
  });
});
