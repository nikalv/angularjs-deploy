import { Injectable } from 'angular-ts-decorators';

@Injectable()
export class  PatientsContainerService {
  static $inject = ['$http'];  
  constructor(private $http: ng.IHttpService) {}
  public getPatients() {
    return this.$http.get('patients.json');
  }

  public getDefaultPaient() {
    return {
      id: 0,
      email: '',
      firstName: '',
      lastName: '',
      sex: '',
      birthData: '',
      deathData: '',
      business: '',
      city: '',
      state: '',
      street: '',
      house: '',
      zip: '',
      phone: '',
      workPhone: '',
      submissionDate: '',
      comments: '',
      medication: {
        "active": [],
        "history": []
      }
      ,
      diagnoses: {
        "active": [],
        "history": []
      }
    };
  }
}

