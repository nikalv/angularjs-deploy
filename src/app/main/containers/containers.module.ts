import * as angular from 'angular';
import { PatientsContainerModule } from './patients-container/patients-container.module';
import { viewportModule } from './viewport/viewport.module';

export const containersModule = angular
  .module('app.main.containers', [
    PatientsContainerModule.name,
    viewportModule
  ])
  .name;
